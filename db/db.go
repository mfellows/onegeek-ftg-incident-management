package db

import (
	"log"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/mattes/migrate/migrate"

	// Postgres driver
	_ "github.com/lib/pq"
	_ "github.com/mattes/migrate/driver/postgres"
)

// Manager is an abstraction to get different DB implementations
type Manager interface {
	Setup(connectionString string) *sqlx.DB
}

// GetDatabase returns a database handler for a given type.
func GetDatabase(connectionString string) (db *sqlx.DB) {
	// Create the DB
	var manager Manager
	dbType := strings.Split(connectionString, ":")[0]

	switch dbType {
	case "postgres":
		manager = &PostgresManager{}
		db = manager.Setup(connectionString)
	default:
		log.Fatalln("[ERROR] No database driver supplied")
	}

	return db
}

// Migrate runs DB migrations.
func Migrate(connectionString string) []error {
	allErrors, ok := migrate.UpSync(connectionString, "./db/migrations")
	if !ok {
		log.Println("[ERROR] unable to run database migrations:", allErrors)
	}
	return allErrors
}
