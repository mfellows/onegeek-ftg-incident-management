package main

import (
	"io/ioutil"
	"log"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"

	// Postgres driver
	_ "github.com/lib/pq"
)

func main() {
	log.Println("[DEBUG] Running DB seeding")
	c := config.NewConfig()
	seed, _ := ioutil.ReadFile("db/seeds/seed.sql")
	// seed, _ := ioutil.ReadFile("db/seeds/2017.sql")

	_, err := c.DB.Exec(string(seed))
	if err != nil {
		log.Fatalln("[ERROR] unable to setup db seed:", err)
	}

	// _, err = c.DB.Exec(string(seed2))
	// if err != nil {
	// 	log.Fatalln("[ERROR] unable to setup db seed:", err)
	// }
}
