CREATE TABLE teacher (
    id SERIAL PRIMARY KEY,
    code TEXT,
    first_name TEXT,
    last_name TEXT,
    email TEXT
);
