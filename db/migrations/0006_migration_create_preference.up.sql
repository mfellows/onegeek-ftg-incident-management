CREATE TABLE preference (
    id SERIAL PRIMARY KEY,
    school_id INTEGER REFERENCES school (id),
    type TEXT NOT NULL,
    value TEXT NOT NULL
);
