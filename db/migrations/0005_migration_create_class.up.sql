CREATE TABLE class (
    id SERIAL PRIMARY KEY,
    teacher_id INTEGER REFERENCES teacher (id),
    school_id INTEGER REFERENCES school (id),
    name TEXT NULL,
    year INTEGER
);

CREATE TABLE class_students (
    class_id INTEGER REFERENCES class (id),
    student_id INTEGER REFERENCES student (id)
);
