CREATE TABLE incident (
    id SERIAL PRIMARY KEY,
    summary TEXT,
    description TEXT NULL,
    follow_up BOOLEAN,
    location TEXT,
    action_taken TEXT NULL,
    start_time TIMESTAMP,
    end_time TIMESTAMP
);

CREATE TABLE incident_student (
  incident_id INTEGER REFERENCES incident (id),
  student_id INTEGER REFERENCES student (id),
  PRIMARY KEY(incident_id, student_id)
)
