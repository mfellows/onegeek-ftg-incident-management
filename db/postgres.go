package db

import (
	"log"

	"github.com/jmoiron/sqlx"
	// Postgres driver
	_ "github.com/lib/pq"
)

// PostgresManager implements the SQLLite abstraction.
type PostgresManager struct {
	db *sqlx.DB
}

// Setup the SQLLite connection.
func (s *PostgresManager) Setup(connectionString string) *sqlx.DB {
	db, err := sqlx.Open("postgres", connectionString)
	if err != nil {
		log.Fatalln("[ERROR] unable to connect to db:", err)
	}
	s.db = db
	return db
}
