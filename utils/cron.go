package utils

import (
	"fmt"
	"log"
	"time"
)

// Cron is an overly simplistic task runner.
type Cron struct {
	// When states the first time the job should run
	When time.Time

	// Next is added to the current "when" after a job is run.
	// e.g. to run the job every 24 hours, this might be time.Hour * 24
	Next time.Duration

	// Name of the job
	Name string

	// Job function to run
	Job func()
}

// Start starts up the job runner.
func (c *Cron) Start() {
	fmt.Println("[INFO] starting cron service for", c.Name)
	when := c.When.Sub(time.Now())
	fmt.Println("[INFO] calculating wait period of", when)

	for {
		select {
		case <-time.After(when):
			fmt.Println("Job running! Time is after", c.When)
			c.run(c.Job)
			when = c.Next
		}
	}
}

//runs the actual "cron" job.
func (c *Cron) run(job func()) {
	log.Println("[INFO] running scheduled job", c.Name)
	job()
}
