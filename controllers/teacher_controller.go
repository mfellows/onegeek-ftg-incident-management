package controllers

import (
	"log"
	"net/http"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"github.com/gin-gonic/gin"
)

// TeachersController wraps up teacher related routes.
type TeachersController struct {
	config            *config.Config
	teacherRepository models.TeacherRepository
}

// NewTeachersController returns a new controller.
func NewTeachersController(config *config.Config) *TeachersController {
	return &TeachersController{
		config:            config,
		teacherRepository: models.NewTeacherRepository(),
	}
}

// List Teachers Route.
func (s *TeachersController) List(c *gin.Context) {
	teachers, err := s.teacherRepository.List()
	if err == nil {
		c.JSON(200, teachers)
	} else {
		log.Println("[DEBUG] unable to list teachers:", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// Get Teachers Route.
func (s *TeachersController) Get(c *gin.Context) {
	id := c.Param("id")
	teacher, err := s.teacherRepository.Get(id)
	if err == nil {
		c.JSON(200, teacher)
	} else {
		log.Println("[DEBUG] unable to find teacher with id", id, ":", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// Create Teacher Route.
func (s *TeachersController) Create(c *gin.Context) {
	var teacher models.Teacher

	if c.Bind(&teacher) == nil {
		err := s.teacherRepository.Create(teacher)
		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			log.Println("[DEBUG] unable to create teacher ", teacher, ":", err)
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}

// Update Teacher Route.
func (s *TeachersController) Update(c *gin.Context) {
	var teacher models.Teacher

	if c.Bind(&teacher) == nil {
		err := s.teacherRepository.Update(teacher)

		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}
