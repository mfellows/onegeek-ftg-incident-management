package controllers

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"strings"
	"time"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/service"
	"github.com/gin-gonic/gin"
)

// TODO: remove hard coded school
var schoolID = "1"

// BatchController wraps up student related routes.
type BatchController struct {
	config               *config.Config
	incidentRepository   models.IncidentRepository
	teacherRepository    models.TeacherRepository
	preferenceRepository models.PreferenceRepository
	mailService          *service.MailService
}

// NewBatchController returns a new controller.
func NewBatchController(config *config.Config, mailSvc *service.MailService) *BatchController {
	return &BatchController{
		config:               config,
		incidentRepository:   models.NewIncidentRepository(),
		teacherRepository:    models.NewTeacherRepository(),
		mailService:          mailSvc,
		preferenceRepository: models.NewPreferenceRepository(),
	}
}

// SendDigestCommunications sends out daily emails to a specified set of
// contact(s) in the school.
func (s *BatchController) SendDigestCommunications(c *gin.Context) {

	// TODO: currently assumes only one school. Make this more generic.
	incidents, err := s.incidentRepository.GetIncidentsForEndOfDay()
	if err == nil {
		m := s.generateBatchEmails(incidents)
		if err = s.mailService.SendMessage(*m); err != nil {
			log.Println("[ERROR] unable to send mail: ", err)
		}

		c.JSON(200, gin.H{})
	}
}

type digestTemplate struct {
	Incidents []incidentTemplate
	Contact   models.Contact
	Empty     bool
}
type incidentTemplate struct {
	Students    string
	StartTime   string
	EndTime     string
	Location    string
	Summary     string
	Description string
	FollowUp    bool
}

func (s *BatchController) generateBatchEmails(incidents []*models.Incident) *models.Mail {
	log.Println("[DEBUG] generating digest email")
	var err error
	var tmpl *template.Template

	// TODO: remove hard coded school ID
	principal, _ := s.teacherRepository.FindPrincipalBySchoolID(schoolID)
	principalContact := models.Contact{
		Name:    principal.FirstName,
		Address: principal.Email,
	}

	// Add CC's from default contacts
	// TODO: remove hard coded school ID
	contactsPreference, _ := s.preferenceRepository.GetPreferencesByTypeAndSchoolID(models.ContactType, schoolID)
	var cc []models.Contact
	for _, c := range contactsPreference {
		cc = append(cc, models.Contact{Name: c.Value, Address: c.Value})
	}

	digest := digestTemplate{
		Empty:   len(incidents) == 0,
		Contact: principalContact,
	}

	for _, i := range incidents {
		students := make([]string, len(i.Students))
		for j, student := range i.Students {
			students[j] = fmt.Sprintf("%s %s", student.FirstName, student.LastName)
		}

		digest.Incidents = append(digest.Incidents, incidentTemplate{
			strings.Join(students, ","),
			timeInMelbourne(i.StartTime).Format("Mon Jan 2, 15:04:05"),
			timeInMelbourne(i.EndTime).Format("Mon Jan 2, 15:04:05"),
			i.Location,
			i.Summary,
			i.Description.String,
			i.FollowUp,
		})
	}

	tmpl, err = template.New("incident-digest.html").ParseFiles("templates/incident-digest.html")
	if err != nil {
		log.Println("[ERROR] error parsing HTML template", err)
		return nil
	}

	body := bytes.NewBufferString("")
	err = tmpl.Execute(body, digest)
	if err != nil {
		log.Println("[ERROR] error executing HTML template", err)
		return nil
	}

	return &models.Mail{
		Subject: fmt.Sprintf("%sDaily yard incident notification for %v", s.config.MailSubjectPrefix, time.Now().Format("Mon Jan 2, 15:04:05")),
		Body:    body.String(),
		To:      []models.Contact{principalContact},
		Cc:      cc,
	}
}
