package controllers

import (
	"log"
	"net/http"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/service"
	"github.com/gin-gonic/gin"
)

// MailController wraps up mail related routes.
type MailController struct {
	config *config.Config
	// mailRepository models.MailRepository
	mailService *service.MailService
}

// NewMailController returns a new controller.
func NewMailController(config *config.Config) *MailController {
	return &MailController{
		config:      config,
		mailService: service.NewMailService(),
	}
}

// Create MailRoute.
func (s *MailController) Create(c *gin.Context) {
	var mail models.Mail
	// This will infer what binder to use depending on the content-type header.
	if c.Bind(&mail) == nil {
		log.Println("[DEBUG] received mail request:", mail)
		s.mailService.SendMessage(mail)

		var err error
		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			log.Println("[DEBUG] unable to create mail ", mail, ":", err)
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}
