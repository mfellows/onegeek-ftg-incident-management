package controllers

import (
	"log"
	"net/http"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"github.com/gin-gonic/gin"
)

// SchoolController wraps up school related routes.
type SchoolController struct {
	config           *config.Config
	schoolRepository models.SchoolRepository
}

// NewSchoolController returns a new controller.
func NewSchoolController(config *config.Config) *SchoolController {
	return &SchoolController{
		config:           config,
		schoolRepository: models.NewSchoolRepository(),
	}
}

// List School Route.
func (s *SchoolController) List(c *gin.Context) {
	schools, err := s.schoolRepository.List()
	if err == nil {
		c.JSON(200, schools)
	} else {
		log.Println("[DEBUG] unable to list schools:", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// Get School Route.
func (s *SchoolController) Get(c *gin.Context) {
	id := c.Param("id")
	school, err := s.schoolRepository.Get(id)
	if err == nil {
		c.JSON(200, school)
	} else {
		log.Println("[DEBUG] unable to find school with id", id, ":", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// Create SchoolRoute.
func (s *SchoolController) Create(c *gin.Context) {
	var school models.School
	if c.Bind(&school) == nil {
		err := s.schoolRepository.Create(school)
		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			log.Println("[DEBUG] unable to create school ", school, ":", err)
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}

// Update SchoolRoute.
func (s *SchoolController) Update(c *gin.Context) {
	var school models.School
	if c.Bind(&school) == nil {
		err := s.schoolRepository.Update(school)

		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}

// Delete Incident Route.
func (s *SchoolController) Delete(c *gin.Context) {
	id := c.Param("id")
	err := s.schoolRepository.Delete(id)

	if err == nil {
		c.JSON(200, gin.H{})
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
	}
}
