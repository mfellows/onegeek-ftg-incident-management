package controllers

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"github.com/gin-gonic/gin"
)

// ClassRoomController wraps up class related routes.
type ClassRoomController struct {
	config          *config.Config
	classRepository models.ClassRoomRepository
}

// NewClassRoomController returns a new controller.
func NewClassRoomController(config *config.Config) *ClassRoomController {
	return &ClassRoomController{
		config:          config,
		classRepository: models.NewClassRoomRepository(),
	}
}

// ListCurrentYear gets the classes for the current year
func (s *ClassRoomController) ListCurrentYear(c *gin.Context) {
	year := time.Now().Year()

	classes, err := s.classRepository.ListByYear(year)
	if err == nil {
		c.JSON(200, classes)
		return
	}

	log.Println("[DEBUG] unable to list classes for year:", year, "error:", err)
	c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
}

// List ClassRoom Route.
func (s *ClassRoomController) List(c *gin.Context) {
	year := c.Param("year")
	var yearInt int
	var err error
	var classes []models.ClassRoom

	if yearInt, err = strconv.Atoi(year); err == nil {
		classes, err = s.classRepository.ListByYear(yearInt)
		if err == nil {
			c.JSON(200, classes)
			return
		}
	}

	log.Println("[DEBUG] unable to list classes:", err)
	c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
}

// Get ClassRoom Route.
func (s *ClassRoomController) Get(c *gin.Context) {
	id := c.Param("id")
	class, err := s.classRepository.Get(id)
	if err == nil {
		c.JSON(200, class)
	} else {
		log.Println("[DEBUG] unable to find class with id", id, ":", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// Create ClassRoomRoute.
func (s *ClassRoomController) Create(c *gin.Context) {
	var class models.ClassRoom
	if c.Bind(&class) == nil {
		err := s.classRepository.Create(class)
		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			log.Println("[DEBUG] unable to create class ", class, ":", err)
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}

// Update ClassRoomRoute.
func (s *ClassRoomController) Update(c *gin.Context) {
	var class models.ClassRoom
	if c.Bind(&class) == nil {
		err := s.classRepository.Update(class)

		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}

// Delete Incident Route.
func (s *ClassRoomController) Delete(c *gin.Context) {
	id := c.Param("id")
	err := s.classRepository.Delete(id)

	if err == nil {
		c.JSON(200, gin.H{})
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
	}
}
