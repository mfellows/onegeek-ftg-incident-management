package controllers

import (
	"net/http"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"github.com/gin-gonic/gin"
)

// IncidentsController wraps up student related routes.
type IncidentsController struct {
	config             *config.Config
	incidentRepository models.IncidentRepository
	studentRepository  models.StudentRepository
}

// NewIncidentsController returns a new controller.
func NewIncidentsController(config *config.Config) *IncidentsController {
	return &IncidentsController{
		config:             config,
		incidentRepository: models.NewIncidentRepository(),
	}
}

// ListToday Incidents that happened today!
func (s *IncidentsController) ListToday(c *gin.Context) {
	incidents, err := s.incidentRepository.GetIncidentsForEndOfDay()

	if err == nil {
		c.JSON(200, &incidents)
	} else {
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found", "error": err.Error()})
	}
}

// List Incidents.
func (s *IncidentsController) List(c *gin.Context) {
	incidents, err := s.incidentRepository.List()

	if err == nil {
		c.JSON(200, &incidents)
	} else {
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found", "error": err.Error()})
	}
}

// Create Incident Route.
func (s *IncidentsController) Create(c *gin.Context) {
	var incident models.Incident

	if c.Bind(&incident) == nil {
		err := s.incidentRepository.Create(incident)

		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}

// Get Incident Route.
func (s *IncidentsController) Get(c *gin.Context) {
	id := c.Param("id")
	incident, err := s.incidentRepository.Get(id)

	if err == nil {
		c.JSON(200, incident)
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
	}
}

// Delete Incident Route.
func (s *IncidentsController) Delete(c *gin.Context) {
	id := c.Param("id")
	err := s.incidentRepository.Delete(id)

	if err == nil {
		c.JSON(200, gin.H{})
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
	}
}

// Update Incident Route.
func (s *IncidentsController) Update(c *gin.Context) {
	var incident models.Incident

	if c.Bind(&incident) == nil {
		err := s.incidentRepository.Update(incident)

		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}
