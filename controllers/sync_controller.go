package controllers

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"time"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/service"
	"github.com/gin-gonic/gin"
)

// SyncController wraps up student related routes.
type SyncController struct {
	config               *config.Config
	incidentRepository   models.IncidentRepository
	studentRepository    models.StudentRepository
	teacherRepository    models.TeacherRepository
	preferenceRepository models.PreferenceRepository
	mailService          *service.MailService
}

// NewSyncController returns a new controller.
func NewSyncController(config *config.Config, mailSvc *service.MailService) *SyncController {
	return &SyncController{
		config:               config,
		incidentRepository:   models.NewIncidentRepository(),
		preferenceRepository: models.NewPreferenceRepository(),
		studentRepository:    models.NewStudentRepository(),
		teacherRepository:    models.NewTeacherRepository(),
		mailService:          mailSvc,
	}
}

func timeInMelbourne(t time.Time) time.Time {
	loc, err := time.LoadLocation("Australia/Melbourne")
	if err != nil {
		panic(err)
	}
	return t.In(loc)
}

func (s *SyncController) mailNewStudents(i models.Incident, students []models.Student) []models.Mail {
	var messages []models.Mail
	var teacher models.Teacher
	var err error
	var tmpl *template.Template
	var cc []models.Contact

	// Add CC's from default contacts
	// TODO: remove hard coded school ID
	contactsPreference, _ := s.preferenceRepository.GetPreferencesByTypeAndSchoolID(models.ContactType, "1")
	for _, c := range contactsPreference {
		cc = append(cc, models.Contact{Name: c.Value, Address: c.Value})
	}

	for _, student := range students {
		if teacher, err = s.teacherRepository.FindTeacherByStudentID(fmt.Sprintf("%d", student.ID)); err == nil {
			log.Println("[DEBUG] creating mail message for student:", student, ", teacher:", teacher)

			data := struct {
				Teacher     string
				Student     string
				RaisedBy    string
				StartTime   string
				EndTime     string
				Location    string
				Summary     string
				Description string
				FollowUp    bool
			}{
				teacher.FirstName,
				student.FirstName + " " + student.LastName,
				i.Teacher.FirstName + " " + i.Teacher.LastName,
				timeInMelbourne(i.StartTime).Format("Mon Jan 2, 15:04:05"),
				timeInMelbourne(i.EndTime).Format("Mon Jan 2, 15:04:05"),
				i.Location,
				i.Summary,
				i.Description.String,
				i.FollowUp,
			}

			tmpl, err = template.New("incident-single.html").ParseFiles("templates/incident-single.html")

			if err != nil {
				log.Println("[ERROR] error parsing HTML template", err)
				return nil
			}
			body := bytes.NewBufferString("")
			err = tmpl.Execute(body, data)
			if err != nil {
				log.Println("[ERROR] error executing HTML template", err)
				return nil
			}

			messages = append(messages, models.Mail{
				Subject: fmt.Sprintf("%sYard incident notification for %s %s @%v", s.config.MailSubjectPrefix, student.FirstName, student.LastName, timeInMelbourne(i.StartTime).Format("Mon Jan 2, 15:04:05")),
				Body:    body.String(),
				Cc:      cc,
				To: []models.Contact{models.Contact{
					Name:    teacher.FirstName,
					Address: teacher.Email,
				}}})
		} else {
			log.Println("[ERROR] unable to find teacher for student:", err)
		}
	}
	return messages
}

// Sync Incidents.
func (s *SyncController) Sync(c *gin.Context) {
	var incidents []models.Incident
	var messages []models.Mail
	err := c.Bind(&incidents)

	if err == nil {
		for _, i := range incidents {
			if i.ID > 0 {
				// Find new students on the incident that need to be added to record and mailed...
				var newStudents []models.Student
				newStudents, err = s.studentRepository.FindNewStudentsForIncident(i, i.Students)

				if err != nil {
					log.Println("[ERROR] unable to determine new students", err)
					c.JSON(400, gin.H{"status": "bad request", "error": err.Error()})
					return
				}
				log.Println("[DEBUG] identified new students for incident:", newStudents)
				messages = append(messages, s.mailNewStudents(i, newStudents)...)

				// Update the incident
				err = s.incidentRepository.Update(i)
				if err != nil {
					log.Println("[ERROR] unable to update incident:", err)
					c.JSON(400, gin.H{"status": "bad request", "error": err.Error()})
					return
				}

			} else {
				err = s.incidentRepository.Create(i)
				if err != nil {
					log.Println("[ERROR] unable to create new incident", err)
					c.JSON(400, gin.H{"status": "bad request", "error": err.Error()})
					return
				}

				ids := s.studentRepository.ExtractStudentIds(i.Students)
				log.Println("[DEBUG] ids of students to email:", ids)
				var students []models.Student
				students, err = s.studentRepository.FindByIDs(ids)
				log.Println("[DEBUG] students to email:", students)

				if err != nil {
					log.Println("[ERROR] unable to create incident:", err)
					c.JSON(400, gin.H{"status": "bad request", "error": err.Error()})
					return
				}
				messages = append(messages, s.mailNewStudents(i, students)...)
			}

			if err != nil {
				c.JSON(400, gin.H{"status": "bad request", "error": err.Error()})
				return
			}
		}

		// Send mail!
		for _, m := range messages {
			if err = s.mailService.SendMessage(m); err != nil {
				log.Println("[ERROR] unable to send mail: ", err)
			}
		}

		c.JSON(200, gin.H{"status": "completed"})
	} else {
		log.Println("Could not parse. doh. Error: ", err)
		c.JSON(400, gin.H{"status": "bad request"})
	}
}
