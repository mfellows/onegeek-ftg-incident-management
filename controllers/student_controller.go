package controllers

import (
	"log"
	"net/http"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"github.com/gin-gonic/gin"
)

// StudentsController wraps up student related routes.
type StudentsController struct {
	config             *config.Config
	studentRepository  models.StudentRepository
	incidentRepository models.IncidentRepository
	teacherRepository  models.TeacherRepository
}

// NewStudentsController returns a new controller.
func NewStudentsController(config *config.Config) *StudentsController {
	return &StudentsController{
		config:             config,
		studentRepository:  models.NewStudentRepository(),
		incidentRepository: models.NewIncidentRepository(),
		teacherRepository:  models.NewTeacherRepository(),
	}
}

// List Students Route.
func (s *StudentsController) List(c *gin.Context) {
	students, err := s.studentRepository.List()
	if err == nil {
		c.JSON(200, students)
	} else {
		log.Println("[DEBUG] unable to list students:", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// Get Students Route.
func (s *StudentsController) Get(c *gin.Context) {
	id := c.Param("id")
	student, err := s.studentRepository.Get(id)
	if err == nil {
		c.JSON(200, student)
	} else {
		log.Println("[DEBUG] unable to find student with id", id, ":", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// Create Student Route.
func (s *StudentsController) Create(c *gin.Context) {
	var student models.Student

	// This will infer what binder to use depending on the content-type header.
	if c.Bind(&student) == nil {
		// Named queries, using `:name` as the bindvar.  Automatic bindvar support
		// which takes into account the dbtype based on the driverName on sqlx.Open/Connect
		err := s.studentRepository.Create(student)
		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			log.Println("[DEBUG] unable to create student ", student, ":", err)
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}

// Update Student Route.
func (s *StudentsController) Update(c *gin.Context) {
	var student models.Student

	if c.Bind(&student) == nil {
		err := s.studentRepository.Update(student)

		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}

// GetIncidentsByStudent finds all Incidents for a given student Route.
func (s *StudentsController) GetIncidentsByStudent(c *gin.Context) {
	id := c.Param("id")

	incidents, err := s.incidentRepository.GetIncidentsByStudent(id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
	} else if len(incidents) == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	} else {
		c.JSON(200, incidents)
	}
}

// GetTeacherByStudent Teachers Route.
func (s *StudentsController) GetTeacherByStudent(c *gin.Context) {
	id := c.Param("id")
	teacher, err := s.teacherRepository.FindTeacherByStudentID(id)
	if err == nil {
		c.JSON(200, teacher)
	} else {
		log.Println("[DEBUG] unable to find teachers for student:", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}
