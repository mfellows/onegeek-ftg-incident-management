package controllers

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"github.com/gin-gonic/gin"
)

// PreferencesController wraps up preferences related routes.
type PreferencesController struct {
	config                *config.Config
	preferencesRepository models.PreferenceRepository
}

// NewPreferencesController returns a new controller.
func NewPreferencesController(config *config.Config) *PreferencesController {
	return &PreferencesController{
		config:                config,
		preferencesRepository: models.NewPreferenceRepository(),
	}
}

// ListBySchool lists preferences route by school id
func (s *PreferencesController) ListBySchool(c *gin.Context) {
	id := c.Param("id")
	preferences, err := s.preferencesRepository.GetPreferencesBySchoolID(id)
	if err == nil {
		c.JSON(200, preferences)
	} else {
		log.Println("[DEBUG] unable to find preferences for school with id", id, ":", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// List Preferences Route.
func (s *PreferencesController) List(c *gin.Context) {
	preferencess, err := s.preferencesRepository.List()
	if err == nil {
		c.JSON(200, preferencess)
	} else {
		log.Println("[DEBUG] unable to list preferencess:", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// Get Preferences Route.
func (s *PreferencesController) Get(c *gin.Context) {
	id := c.Param("id")
	preference, err := s.preferencesRepository.Get(id)
	if err == nil {
		c.JSON(200, preference)
	} else {
		log.Println("[DEBUG] unable to find preferences with id", id, ":", err)
		c.JSON(http.StatusNotFound, gin.H{"status": "file not found"})
	}
}

// Delete Preference Route.
func (s *PreferencesController) Delete(c *gin.Context) {
	id := c.Param("id")
	err := s.preferencesRepository.Delete(id)

	if err == nil {
		c.JSON(200, gin.H{})
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
	}
}

// Create Preference Route.
func (s *PreferencesController) Create(c *gin.Context) {
	var preference models.Preference
	var err error

	if err = c.Bind(&preference); err == nil {
		fmt.Println(preference)
		err = s.preferencesRepository.Create(preference)
		if err == nil {
			c.JSON(200, gin.H{})
			return
		}
	}

	log.Println("[DEBUG] unable to create preferences ", preference, ":", err)
	c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
}

// Update Preference Route.
func (s *PreferencesController) Update(c *gin.Context) {
	var preference models.Preference

	if c.Bind(&preference) == nil {
		err := s.preferencesRepository.Update(preference)

		if err == nil {
			c.JSON(200, gin.H{})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"status": "bad request", "error": err.Error()})
		}
	}
}
