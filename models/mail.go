package models

// Mail is currently an abstraction over Mailgun. Put mail details into this
// object and use the MailService to make it appear in somebody's inbox.
type Mail struct {
	To      []Contact
	Cc      []Contact
	Body    string
	Subject string
}
