package models

// Repository is a remote access interface,
// it could be a DB, Remote API etc.
//
// TODO: Make all repositories accessible in the context of a Txn or standard DB
//       connection pool. i.e. should they all have a private member of a interface
//       type?
type Repository interface {
	Get(id string) interface{}
	List() interface{}
	Create(entity interface{}) interface{}
	Delete(id string) interface{}
	// GetTxnRepository(txn sqlx.Tx)
}
