package models

// Contact is the To/From address that an email will be sent to/from.
type Contact struct {
	ID      int    `db:"id" json:"id"`
	Name    string `db:"name" json:"name"`
	Address string `db:"email" json:"email"`
}
