package models

import (
	"log"
	"strconv"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"github.com/jmoiron/sqlx"
)

// School is where it all happens ;)
type School struct {
	ID        int      `db:"id" json:"id"`
	Name      string   `db:"name" json:"name"`
	Principal NullInt  `db:"principal" json:"principal"`
	Classes   []School `json:"-"`
}

// SchoolRepository provides convenience APIs for accessing Schools.
type SchoolRepository struct {
	c *config.Config
}

// NewSchoolRepository provides a new remote School interface.
func NewSchoolRepository() SchoolRepository {
	return SchoolRepository{c: config.GetGlobalConfig()}
}

// Get a school by ID.
func (s SchoolRepository) Get(id string) (school School, err error) {
	var idInt int
	if idInt, err = strconv.Atoi(id); err == nil {
		err = s.c.DB.Get(&school, "SELECT * FROM school where id = $1", idInt)
	}

	return school, err
}

// Delete a school by ID.
func (s SchoolRepository) Delete(id string) (err error) {
	var idInt int
	if idInt, err = strconv.Atoi(id); err == nil {
		_, err = s.c.DB.Exec("DELETE FROM school where id = $1", idInt)
	}

	return err
}

// FindByIDs finds Schools by their IDs.
func (s SchoolRepository) FindByIDs(ids []int) ([]School, error) {
	var schools []School
	query, args, err := sqlx.In("SELECT * FROM school WHERE id IN (?)", ids)
	if err != nil {
		log.Println("[ERROR]", err)
		return nil, err
	}
	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&schools, query, args...)

	return schools, err
}

// List all schools.
func (s SchoolRepository) List() (schools []School, err error) {
	err = s.c.DB.Select(&schools, "SELECT * FROM school")
	return schools, err
}

// Create a new School in the database.
func (s SchoolRepository) Create(school School) (err error) {
	_, err = s.c.DB.NamedExec(`INSERT INTO school (name, principla) VALUES (:name, :principal)`, school)
	return
}

// Update a School in the database.
func (s SchoolRepository) Update(school School) (err error) {
	_, err = s.c.DB.NamedExec(`UPDATE school SET name = :name, principal = :principal WHERE id = :id`, school)
	return
}

// GetSchoolsByIds gets a set of Schools by their IDs.
func (s SchoolRepository) GetSchoolsByIds(schoolIds []int) ([]School, error) {
	var err error

	var schools []School
	var query string
	var args []interface{}

	query, args, err = sqlx.In("SELECT * FROM school WHERE id IN (?)", schoolIds)
	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&schools, query, args...)

	return schools, err
}
