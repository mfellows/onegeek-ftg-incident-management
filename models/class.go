package models

import (
	"database/sql/driver"
	"fmt"
	"log"
	"strconv"
	"strings"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"github.com/jmoiron/sqlx"
)

type sqlIntSlice []int

func (p *sqlIntSlice) Scan(src interface{}) error {
	switch src := src.(type) {
	case []byte:
		items := strings.Split(string(src[1:len(src)-1]), ",")
		a := make([]int, len(items))
		for i, v := range items {
			a[i], _ = strconv.Atoi(v)
		}
		*p = a
	default:
		return fmt.Errorf("sqlIntSlice: cannot parse %T to []int", src)
	}

	return nil
}

func (p *sqlIntSlice) Value() (driver.Value, error) {
	return nil, fmt.Errorf("sqlIntSlice: input array not supported")
}

// ClassRoom is as self-explanatory as you think it.
type ClassRoom struct {
	ID        int         `db:"id" json:"id"`
	ClassRoom string      `db:"name" json:"name"`
	Year      int         `db:"year" json:"year"`
	TeacherID int         `db:"teacher_id" json:"teacher_id"`
	Students  sqlIntSlice `json:"students"`
	SchoolID  int         `db:"school_id" json:"school_id"`
}

// ClassRoomRepository provides convenience APIs for accessing ClassRooms.
type ClassRoomRepository struct {
	c *config.Config
}

// NewClassRoomRepository provides a new remote ClassRoom interface.
func NewClassRoomRepository() ClassRoomRepository {
	return ClassRoomRepository{c: config.GetGlobalConfig()}
}

// Get a class by ID.
func (s ClassRoomRepository) Get(id string) (class ClassRoom, err error) {
	var idInt int
	if idInt, err = strconv.Atoi(id); err == nil {
		err = s.c.DB.Get(&class, "SELECT c.*, cs.student_id FROM class as c, class_students as cs where id = $1 and c.id = cs.class_id", idInt)
	}

	return class, err
}

// Delete a class by ID.
func (s ClassRoomRepository) Delete(id string) (err error) {
	var idInt int
	if idInt, err = strconv.Atoi(id); err == nil {
		_, err = s.c.DB.Exec("DELETE FROM class where id = $1", idInt)
	}

	return err
}

// FindByIDs finds ClassRooms by their IDs.
func (s ClassRoomRepository) FindByIDs(ids []int) ([]ClassRoom, error) {
	var classRooms []ClassRoom
	query, args, err := sqlx.In("SELECT * FROM class WHERE id IN (?)", ids)
	if err != nil {
		log.Println("[ERROR]", err)
		return nil, err
	}
	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&classRooms, query, args...)

	return classRooms, err
}

// List all classRooms.
func (s ClassRoomRepository) List() (classRooms []ClassRoom, err error) {
	err = s.c.DB.Select(&classRooms, "SELECT c.*, array_agg(cs.student_id) as students FROM class as c, class_students as cs WHERE c.id = cs.class_id GROUP BY c.id ORDER BY c.id ASC")
	return classRooms, err
}

// ListByYear lists all classRooms in a year
func (s ClassRoomRepository) ListByYear(year int) (classRooms []ClassRoom, err error) {
	err = s.c.DB.Select(&classRooms, "SELECT c.*, array_agg(cs.student_id) as students FROM class as c, class_students as cs WHERE c.id = cs.class_id AND c.year = $1 GROUP BY c.id ORDER BY c.id ASC", year)
	return classRooms, err
}

// Create a new ClassRoom in the database.
func (s ClassRoomRepository) Create(class ClassRoom) (err error) {
	_, err = s.c.DB.NamedExec(`INSERT INTO class (name, year, teacher_id, school_id) VALUES (:name, :year, :teacher_id, :school_id)`, class)
	return
}

// Update a ClassRoom in the database.
func (s ClassRoomRepository) Update(class ClassRoom) (err error) {
	_, err = s.c.DB.NamedExec(`UPDATE class SET name = :name, year = :year, teacher_id = :teacher_id, school_id = :school_id WHERE id = :id`, class)
	return
}

// ExtractClassRoomIds extracts out IDs from (likely) sparsely
// populated ClassRoom structs.
func (s ClassRoomRepository) ExtractClassRoomIds(classRooms []ClassRoom) []int {
	classIds := make([]int, len(classRooms))
	for i, s := range classRooms {
		classIds[i] = s.ID
	}
	return classIds
}

// GetClassRoomsByIds gets a set of ClassRooms by their IDs.
func (s ClassRoomRepository) GetClassRoomsByIds(classIds []int) ([]ClassRoom, error) {
	var err error

	var classRooms []ClassRoom
	var query string
	var args []interface{}

	query, args, err = sqlx.In("SELECT * FROM class WHERE id IN (?)", classIds)
	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&classRooms, query, args...)

	return classRooms, err
}
