package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
)

// NullString wraps the sql.NullString to allow nullable DB values that
// can also marshall to JSON.
//
// Implementation borrowed from: https://github.com/guregu/null
type NullString struct {
	sql.NullString
}

// UnmarshalJSON implements json.Unmarshaler.
// It supports string and null input. Blank string input does not produce a null String.
// It also supports unmarshalling a sql.NullString.
func (s *NullString) UnmarshalJSON(data []byte) error {
	var err error
	var v interface{}
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}
	switch x := v.(type) {
	case string:
		s.String = x
	case map[string]interface{}:
		err = json.Unmarshal(data, &s.NullString)
	case nil:
		s.Valid = false
		return nil
	default:
		err = fmt.Errorf("json: cannot unmarshal %v into Go value of type null.String", reflect.TypeOf(v).Name())
	}
	s.Valid = err == nil
	return err
}

// MarshalJSON implements json.Marshaler.
// It will encode null if this String is null.
func (s NullString) MarshalJSON() ([]byte, error) {
	if !s.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(s.String)
}

// NullInt is an nullable int64.
// It does not consider zero values to be null.
// It will decode to null, not zero, if null.
type NullInt struct {
	sql.NullInt64
}

// UnmarshalJSON implements json.Unmarshaler.
// It supports number and null input.
// 0 will not be considered a null Int.
// It also supports unmarshalling a sql.NullInt64.
func (i *NullInt) UnmarshalJSON(data []byte) error {
	var err error
	var v interface{}
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}
	switch v.(type) {
	case float64:
		// Unmarshal again, directly to int64, to avoid intermediate float64
		err = json.Unmarshal(data, &i.Int64)
	case map[string]interface{}:
		err = json.Unmarshal(data, &i.NullInt64)
	case nil:
		i.Valid = false
		return nil
	default:
		err = fmt.Errorf("json: cannot unmarshal %v into Go value of type null.Int", reflect.TypeOf(v).Name())
	}
	i.Valid = err == nil
	return err
}

// MarshalJSON implements json.Marshaler.
// It will encode null if this Int is null.
func (i NullInt) MarshalJSON() ([]byte, error) {
	if !i.Valid {
		return []byte("null"), nil
	}
	return []byte(strconv.FormatInt(i.Int64, 10)), nil
}

// Incident is as self-explanatory as you think it.
type Incident struct {
	ID          int        `db:"id" json:"id"`
	StartTime   time.Time  `db:"start_time" json:"start_time"`
	EndTime     time.Time  `db:"end_time" json:"end_time"`
	Description NullString `db:"description" json:"description"`
	FollowUp    bool       `db:"follow_up" json:"follow_up"`
	Summary     string     `db:"summary" json:"summary"`
	Location    string     `db:"location" json:"location"`
	ActionTaken NullString `db:"action_taken" json:"action_taken"`
	Students    []Student  `json:"students"`
	Teacher     Teacher    `json:"teacher"`
}

// IncidentRepository provides convenience APIs for accessing Incidents.
type IncidentRepository struct {
	c                 *config.Config
	studentRepository StudentRepository
}

// NewIncidentRepository provides a new remote Incident interface.
func NewIncidentRepository() IncidentRepository {
	return IncidentRepository{
		c:                 config.GetGlobalConfig(),
		studentRepository: NewStudentRepository(),
	}
}

func (s IncidentRepository) rowsToIncidents(rows *sqlx.Rows) (incidents []*Incident, err error) {

	// Marshall into one-to-many relationship
	lastID := 0
	var incident *Incident

	for rows.Next() {
		var ID, StudentID int
		var Summary, Location, FirstName, LastName, Gender string
		var Description, ActionTaken, TeacherFirstName, TeacherLastName NullString
		var FollowUp bool
		var StartTime, EndTime time.Time
		var TeacherID NullInt
		var teacher Teacher
		if err = rows.Scan(&ID, &Summary, &Description, &FollowUp, &Location, &ActionTaken, &StartTime, &EndTime, &TeacherID, &FirstName, &LastName, &TeacherFirstName, &TeacherLastName, &Gender, &StudentID); err == nil {
			// New incident, add to list
			if ID != lastID {
				if TeacherID.Valid {
					teacher = Teacher{FirstName: TeacherFirstName.String, LastName: TeacherLastName.String, ID: int(TeacherID.Int64)}
				}
				incident = &Incident{ID: ID, Summary: Summary, Description: Description, Location: Location, FollowUp: FollowUp, StartTime: StartTime, EndTime: EndTime, ActionTaken: ActionTaken, Students: make([]Student, 0), Teacher: teacher}
				incidents = append(incidents, incident)
			}

			// incident already mapped, extract student
			s := Student{ID: StudentID, FirstName: FirstName, LastName: LastName, Gender: Gender}
			incident.Students = append(incident.Students, s)
		} else {
			log.Println("[ERROR] incident repository error: ", err)
			rows.Close()
			return nil, err
		}

		lastID = ID
	}
	return incidents, err
}

func (s IncidentRepository) getAllOrByID(id string) (incidents []*Incident, err error) {
	var rows *sqlx.Rows

	if id == "" {
		rows, err = s.c.DB.Queryx(`SELECT i.*, s.first_name, s.last_name, t.first_name as teacher_first_name, t.last_name as teacher_last_name, s.gender, s.id as s_id FROM incident i INNER JOIN incident_student iss on iss.incident_id = i.id INNER JOIN student s on iss.student_id = s.id LEFT OUTER JOIN teacher t on i.teacher_id = t.id ORDER BY i.id DESC`)
	} else {
		rows, err = s.c.DB.Queryx(`SELECT i.*, s.first_name, s.last_name, t.first_name as teacher_first_name, t.last_name as teacher_last_name, s.gender, s.id as s_id FROM incident i INNER JOIN incident_student iss on iss.incident_id = i.id INNER JOIN student s on iss.student_id = s.id LEFT OUTER JOIN teacher t on i.teacher_id = t.id WHERE i.id = $1`, id)
	}

	if err != nil {
		return nil, err
	}

	return s.rowsToIncidents(rows)
}

// GetIncidentsByStudent finds all incidents for a given student.
func (s IncidentRepository) GetIncidentsByStudent(id string) (incidents []*Incident, err error) {
	var rows *sqlx.Rows

	rows, err = s.c.DB.Queryx(`SELECT i.*, s.first_name, s.last_name, s.gender, s.id as s_id FROM incident as i, student as s, incident_student as iss WHERE iss.incident_id = i.id AND iss.student_id = s.id AND iss.student_id = $1`, id)

	if err != nil {
		return nil, err
	}

	return s.rowsToIncidents(rows)
}

// Get a incident by ID.
func (s IncidentRepository) Get(id string) (incident *Incident, err error) {
	incidents, err := s.getAllOrByID(id)

	if len(incidents) > 0 {
		return incidents[0], err
	}
	return nil, err
}

// List all Incidents.
func (s IncidentRepository) List() (incidents []*Incident, err error) {
	return s.getAllOrByID("")
}

// Create a new Incident.
func (s IncidentRepository) Create(i Incident) error {
	log.Println("[INFO] creating new incident:", i)
	tx, _ := s.c.DB.Beginx()
	var err error

	// TODO: fix this uglyness
	if i.Teacher.ID > 0 {
		err = tx.QueryRowx(`INSERT INTO incident (start_time, end_time, description, follow_up, summary, location, action_taken, teacher_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id`, i.StartTime, i.EndTime, i.Description, i.FollowUp, i.Summary, i.Location, i.ActionTaken, i.Teacher.ID).Scan(&i.ID)
	} else {
		err = tx.QueryRowx(`INSERT INTO incident (start_time, end_time, description, follow_up, summary, location, action_taken) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id`, i.StartTime, i.EndTime, i.Description, i.FollowUp, i.Summary, i.Location, i.ActionTaken).Scan(&i.ID)
	}

	if err != nil {
		log.Println("[ERROR] unable to create incident:", err)
		tx.Rollback()
		return err
	}

	// Attach students to Incident, and queue up mail to be sent!
	err = s.studentRepository.AddStudentsToIncidentTxn(tx, i, i.Students)
	if err != nil {
		log.Println("[ERROR] unable to add students to incident:", err)
		tx.Rollback()
		return err
	}

	err = tx.Commit()

	return err
}

// Update an Incident.
func (s IncidentRepository) Update(i Incident) error {
	log.Println("[INFO] updating existing incident:", i)
	tx, _ := s.c.DB.Beginx()

	// Update the incident
	_, err := tx.NamedExec(`UPDATE incident SET start_time = :start_time, end_time = :end_time, description = :description, action_taken = :action_taken, follow_up = :follow_up, summary = :summary, location = :location WHERE id = :id`, i)
	if err != nil {
		log.Println("[ERROR] unable to update incident:", err)
		tx.Rollback()
		return err
	}

	// Find and remove students on the incident that are no longer on record
	err = s.studentRepository.RemoveStudentsFromIncidentTxn(tx, i, i.Students)
	if err != nil {
		log.Println("[ERROR] unable to remove students from incident", err)
		tx.Rollback()
		return err
	}

	// Find new students on the incident that need to be added to record and mailed...
	newStudents, err := s.studentRepository.FindNewStudentsForIncident(i, i.Students)

	if err != nil {
		log.Println("[ERROR] unable to determine new/updated students", err)
		tx.Rollback()
		return err
	}

	// Attach students to Incident, and queue up mail to be sent!
	err = s.studentRepository.AddStudentsToIncidentTxn(tx, i, newStudents)

	if err != nil {
		log.Println("[ERROR] unable to create incident:", err)
		tx.Rollback()
		return err
	}

	err = tx.Commit()

	return err
}

// Delete an Incident.
func (s IncidentRepository) Delete(id string) error {
	tx, _ := s.c.DB.Beginx()

	// Find and remove students on the incident that are no longer on record
	// err = s.studentRepository.RemoveStudentsFromIncidentTxn(tx, *incident, incident.Students)
	_, err := tx.Exec(`DELETE FROM incident_student WHERE incident_id = $1`, id)
	if err != nil {
		log.Println("[ERROR] unable to remove students from incident", err)
		tx.Rollback()
		return err
	}

	// Update the incident
	_, err = tx.Exec(`DELETE FROM incident WHERE id = $1`, id)
	if err != nil {
		log.Println("[ERROR] unable to delete incident:", err)
		tx.Rollback()
		return err
	}

	err = tx.Commit()

	return err
}

// GetIncidentsForEndOfDay finds all incidents for a given day.
func (s IncidentRepository) GetIncidentsForEndOfDay() (incidents []*Incident, err error) {
	var rows *sqlx.Rows
	rows, err = s.c.DB.Queryx(`SELECT i.*, s.first_name, s.last_name, s.gender, s.id as s_id FROM incident as i, student as s, incident_student as iss WHERE iss.incident_id = i.id AND iss.student_id = s.id AND date(i.start_time) = current_date`)

	if err != nil {
		return nil, err
	}

	return s.rowsToIncidents(rows)
}
