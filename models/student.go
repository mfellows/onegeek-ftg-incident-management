package models

import (
	"log"
	"strconv"

	"github.com/jmoiron/sqlx"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
)

// Student is as self-explanatory as you think it.
type Student struct {
	ID        int    `db:"id" json:"id"`
	FirstName string `db:"first_name" json:"first_name"`
	LastName  string `db:"last_name" json:"last_name"`
	Gender    string `db:"gender" json:"gender"`
}

// ClassStudents is a SQL construct used to join students and class.
type ClassStudents struct {
	ClassRoom
	Student
}

// StudentRepository provides convenience APIs for accessing Students.
type StudentRepository struct {
	c *config.Config
}

// NewStudentRepository provides a new remote Student interface.
func NewStudentRepository() StudentRepository {
	return StudentRepository{c: config.GetGlobalConfig()}
}

// Get a student by ID.
func (s StudentRepository) Get(id string) (student Student, err error) {
	var idInt int
	if idInt, err = strconv.Atoi(id); err == nil {
		err = s.c.DB.Get(&student, "SELECT * FROM student where id = $1", idInt)
	}

	return student, err
}

// FindByIDs finds Students by their IDs.
func (s StudentRepository) FindByIDs(ids []int) ([]Student, error) {
	var students []Student
	query, args, err := sqlx.In("SELECT * FROM student WHERE id IN (?)", ids)
	if err != nil {
		log.Println("[ERROR]", err)
		return nil, err
	}
	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&students, query, args...)

	return students, err
}

// ListBySchool lists all students in a School.
func (s StudentRepository) ListBySchool(id int) (students []Student, err error) {
	err = s.c.DB.Select(&students, "SELECT s.* FROM student s, class c, class_students cs WHERE s.id = cs.student_id AND cs.class_id = c.id AND c.school_id = $1", id)
	return students, err
}

// List all students.
func (s StudentRepository) List() (students []Student, err error) {
	// Filter by school - Security!
	// SELECT s.* FROM student s, class c, class_students cs WHERE s.id = cs.student_id AND cs.class_id = c.id AND c.school_id = $1
	err = s.c.DB.Select(&students, "SELECT * FROM student")
	return students, err
}

// Create a new Student in the database.
func (s StudentRepository) Create(student Student) (err error) {
	_, err = s.c.DB.NamedExec(`INSERT INTO student (first_name,last_name, gender VALUES (:first_name,:last_name,:gender `, student)
	return
}

// Update a Student in the database.
func (s StudentRepository) Update(student Student) (err error) {
	_, err = s.c.DB.NamedExec(`UPDATE student SET first_name = :first_name, last_name = :last_name, xgender = :gender WHERE id = :id`, student)
	return
}

// ExtractStudentIds extracts out IDs from (likely) sparsely
// populated Student structs.
func (s StudentRepository) ExtractStudentIds(students []Student) []int {
	studentIds := make([]int, len(students))
	for i, s := range students {
		studentIds[i] = s.ID
	}
	return studentIds
}

// FindNewStudentsForIncident find students added to the record to insert and send mail to!
func (s StudentRepository) FindNewStudentsForIncident(i Incident, students []Student) ([]Student, error) {
	log.Println("[DEBUG] find new students for incident:", i, students)
	var newStudents []Student
	studentIds := s.ExtractStudentIds(students)

	query, args, err := sqlx.In("SELECT id, first_name, last_name FROM student WHERE id IN (?) AND id NOT IN (SELECT student_id FROM incident_student WHERE incident_id = ? ORDER BY id DESC)", studentIds, i.ID)
	if err != nil {
		log.Println("[ERROR]", err)
		return nil, err
	}

	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&newStudents, query, args...)

	if err != nil {
		return nil, err
	}
	return newStudents, err
}

// RemoveStudentsFromIncidentTxn removes students with a given id from an incident
// within a Transaction.
func (s StudentRepository) RemoveStudentsFromIncidentTxn(tx *sqlx.Tx, i Incident, students []Student) error {
	studentIds := s.ExtractStudentIds(students)
	var removedStudentIds []int
	query, args, err := sqlx.In("DELETE FROM incident_student WHERE incident_id = ? AND student_id NOT IN (?)", i.ID, studentIds)
	if err != nil {
		log.Println("[ERROR]", err)
		return err
	}
	query = tx.Rebind(query)
	err = tx.Select(&removedStudentIds, query, args...)

	return err
}

// RemoveStudentsFromIncident removes students with a given id from an incident.
func (s StudentRepository) RemoveStudentsFromIncident(i Incident, students []Student) error {
	log.Println("[DEBUG] remove students from incident:", i, students)
	studentIds := s.ExtractStudentIds(students)
	var removedStudentIds []int
	query, args, err := sqlx.In("DELETE FROM incident_student WHERE incident_id = ? AND id NOT IN (?)", i.ID, studentIds)
	if err != nil {
		log.Println("[ERROR]", err)
		return err
	}

	log.Println("[DEBUG] DB", s.c.DB)
	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&removedStudentIds, query, args...)

	return err
}

// GetStudentsByIds gets a set of Students by their IDs.
func (s StudentRepository) GetStudentsByIds(studentIds []int) ([]Student, error) {
	var err error

	// Get students related to incident
	var students []Student
	var query string
	var args []interface{}

	query, args, err = sqlx.In("SELECT * FROM student WHERE id IN (?)", studentIds)
	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&students, query, args...)

	return students, err
}

// AddStudentsToIncidentTxn adds a set of students to a given Incident record.
// given a Transaction.
func (s StudentRepository) AddStudentsToIncidentTxn(tx *sqlx.Tx, i Incident, students []Student) error {
	var err error

	for _, student := range students {
		_, err = tx.Exec(`INSERT INTO incident_student (incident_id, student_id) VALUES ($1, $2)`, i.ID, student.ID)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return err
}

// AddStudentsToIncident adds a set of students to a given Incident record.
func (s StudentRepository) AddStudentsToIncident(i Incident, students []Student) error {
	var err error

	for _, student := range students {
		_, err = s.c.DB.Exec(`INSERT INTO incident_student (incident_id, student_id) VALUES ($1, $2)`, i.ID, student.ID)
		if err != nil {
			return err
		}
	}
	return err
}
