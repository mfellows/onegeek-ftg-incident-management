package models

import (
	"errors"
	"strconv"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
)

// Preference for the school.
type Preference struct {
	ID int `db:"id" json:"id"`

	// Type of preference lookup
	Type string `db:"type" json:"type"`

	// SchoolID is the school this preference is attached to
	SchoolID int `db:"school_id" json:"school_id"`

	// Value of the preference
	Value string `db:"value" json:"value"`
}

const (
	// ContactType is a person who will be cc'd on all notifications.
	ContactType = "contact"

	// ActionType is a set of pre-determined actions taken for an incident.
	ActionType = "action"

	// SummaryType is a set of pre-determined summaries for an incident.
	SummaryType = "summary"

	// LocationType is a location in the school where an Incident may happen.
	LocationType = "location"
)

// PreferenceRepository provides convenience APIs for accessing Preferences.
type PreferenceRepository struct {
	c *config.Config
}

// Get a preference by ID.
func (s PreferenceRepository) Get(id string) (preference *Preference, err error) {
	preferences, err := s.getAllOrByID(id)

	if len(preferences) > 0 {
		return &preferences[0], err
	}
	return nil, errors.New("not found")
}

// NewPreferenceRepository provides a new remote Preference interface.
func NewPreferenceRepository() PreferenceRepository {
	c := config.GetGlobalConfig()

	return PreferenceRepository{c: c}
}

func (s PreferenceRepository) getAllOrByID(id string) (preferences []Preference, err error) {
	if id == "" {
		err = s.c.DB.Select(&preferences, `select id, type, value FROM preference`)
	} else {
		err = s.c.DB.Select(&preferences, `select id, type, value FROM preference p WHERE p.id = $1`, id)
	}

	return
}

// GetPreferencesBySchoolID finds the preferences for a given school.
func (s PreferenceRepository) GetPreferencesBySchoolID(id string) (preferences []*Preference, err error) {
	err = s.c.DB.Select(&preferences, `SELECT p.id, p.type, p.value FROM preference p WHERE p.school_id = $1`, id)

	return
}

// GetPreferencesByTypeAndSchoolID finds the preferences for a given school and
// preference type
func (s PreferenceRepository) GetPreferencesByTypeAndSchoolID(prefType, id string) (preferences []*Preference, err error) {
	err = s.c.DB.Select(&preferences, `SELECT p.id, p.type, p.value FROM preference p WHERE p.type = $1 AND p.school_id = $2`, prefType, id)

	return
}

// List all preferences.
func (s PreferenceRepository) List() (preferences []Preference, err error) {
	return s.getAllOrByID("")
}

// Create a new Preference in the database.
func (s PreferenceRepository) Create(preference Preference) (err error) {
	_, err = s.c.DB.NamedExec(`INSERT INTO preference (type,value,school_id) VALUES (:type,:value,:school_id)`, preference)
	return
}

// Delete a new Preference from the database.
func (s PreferenceRepository) Delete(id string) (err error) {
	var idInt int
	if idInt, err = strconv.Atoi(id); err == nil {
		_, err = s.c.DB.Exec(`DELETE FROM preference p WHERE p.id = $1`, idInt)
	}
	return
}

// Update a Preference in the database.
func (s PreferenceRepository) Update(preference Preference) (err error) {
	_, err = s.c.DB.NamedExec(`UPDATE preference SET type = :type, value = :value, school_id = :school_id WHERE id = :id`, preference)
	return
}

// ExtractPreferenceIds extracts out IDs from (likely) sparsely
// populated Preference structs.
func (s PreferenceRepository) ExtractPreferenceIds(preferences []Preference) []int {
	preferenceIds := make([]int, len(preferences))
	for i, s := range preferences {
		preferenceIds[i] = s.ID
	}
	return preferenceIds
}
