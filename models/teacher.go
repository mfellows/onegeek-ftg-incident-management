package models

import (
	"log"
	"strconv"
	"time"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"github.com/jmoiron/sqlx"
)

// Teacher is as self-explanatory as you think it.
type Teacher struct {
	ID        int    `db:"id" json:"id"`
	Code      string `db:"code" json:"code"`
	FirstName string `db:"first_name" json:"first_name"`
	LastName  string `db:"last_name" json:"last_name"`
	Email     string `db:"email" json:"email"`
}

// TeacherRepository provides convenience APIs for accessing Teachers.
type TeacherRepository struct {
	c *config.Config
}

// NewTeacherRepository provides a new remote Teacher interface.
func NewTeacherRepository() TeacherRepository {
	return TeacherRepository{c: config.GetGlobalConfig()}
}

// Get a teacher by ID.
func (s TeacherRepository) Get(id string) (teacher Teacher, err error) {
	var idInt int
	if idInt, err = strconv.Atoi(id); err == nil {
		err = s.c.DB.Get(&teacher, "SELECT * FROM teacher where id = $1", idInt)
	}

	return teacher, err
}

// FindByIDs finds Teachers by their IDs.
func (s TeacherRepository) FindByIDs(ids []int) ([]Teacher, error) {
	var teachers []Teacher
	query, args, err := sqlx.In("SELECT * FROM teacher WHERE id IN (?)", ids)
	if err != nil {
		log.Println("[ERROR]", err)
		return nil, err
	}
	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&teachers, query, args...)

	return teachers, err
}

// FindTeacherByStudentID finds a Teacher by a student in their class.
func (s TeacherRepository) FindTeacherByStudentID(id string) (teacher Teacher, err error) {
	err = s.c.DB.Get(&teacher, "select t.* from teacher as t, class_students as cs, class c where c.teacher_id = t.id and c.id=cs.class_id  AND cs.student_id = $1 AND c.year = $2", id, time.Now().Year())

	return
}

// FindPrincipalBySchoolID finds a Principal by  in their class.
func (s TeacherRepository) FindPrincipalBySchoolID(id string) (principal Teacher, err error) {
	err = s.c.DB.Get(&principal, "SELECT t.* FROM teacher as t, school as s WHERE t.id = s.principal AND s.id = $1", id)

	return
}

// List all teachers.
func (s TeacherRepository) List() (teachers []Teacher, err error) {
	// Filter by school - Security!
	// select t.* from teacher t, class c WHERE t.id = c.teacher_id  AND c.school_id = $1;
	err = s.c.DB.Select(&teachers, "SELECT * FROM teacher")
	return teachers, err
}

// Create a new Teacher in the database.
func (s TeacherRepository) Create(teacher Teacher) (err error) {
	_, err = s.c.DB.NamedExec(`INSERT INTO teacher (code,first_name,last_name,email) VALUES (:code,:first_name,:last_name,:email)`, teacher)
	return
}

// Update a Teacher in the database.
func (s TeacherRepository) Update(teacher Teacher) (err error) {
	_, err = s.c.DB.NamedExec(`UPDATE teacher SET code = :code, first_name = :first_name, last_name = :last_name, email = :email WHERE id = :id`, teacher)
	return
}

// ExtractTeacherIds extracts out IDs from (likely) sparsely
// populated Teacher structs.
func (s TeacherRepository) ExtractTeacherIds(teachers []Teacher) []int {
	teacherIds := make([]int, len(teachers))
	for i, s := range teachers {
		teacherIds[i] = s.ID
	}
	return teacherIds
}

// GetTeachersByIds gets a set of Teachers by their IDs.
func (s TeacherRepository) GetTeachersByIds(teacherIds []int) ([]Teacher, error) {
	var err error

	var teachers []Teacher
	var query string
	var args []interface{}

	query, args, err = sqlx.In("SELECT * FROM teacher WHERE id IN (?)", teacherIds)
	query = s.c.DB.Rebind(query)
	err = s.c.DB.Select(&teachers, query, args...)

	return teachers, err
}
