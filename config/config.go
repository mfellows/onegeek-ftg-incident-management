package config

import (
	"log"
	"os"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/db"

	"github.com/hashicorp/logutils"
	"github.com/jmoiron/sqlx"
	"github.com/kelseyhightower/envconfig"

	// Auto load .env into env vars
	_ "github.com/joho/godotenv/autoload"

	// Postgres SQL Driver
	_ "github.com/lib/pq"
	_ "github.com/mattes/migrate/driver/postgres"
)

// globalConfig contains the current Config for our application
var globalConfig *Config

// Config wraps up the entire application configuration
type Config struct {
	ConnectionString       string `envconfig:"DATABASE_URL" default:"sqlite3://app.db"`
	LogLevel               string `envconfig:"LOG_LEVEL" default:"INFO"`
	Port                   string `envconfig:"PORT" default:"8000"`
	DB                     *sqlx.DB
	RedisURL               string `envconfig:"REDIS_URL"`
	MailAPIKey             string `envconfig:"SENDGRID_API_KEY" default:"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"`
	MailCCRecipientName    string `envconfig:"MAIL_RECIPIENT_CC_NAME" default:"Matt Fellows"`
	MailCCRecipientAddress string `envconfig:"MAIL_RECIPIENT_CC_EMAIL" default:"matt.fellows@onegeek.com.au"`
	MailFromName           string `envconfig:"MAIL_FROM_NAME" default:"Matt Fellows"`
	MailFromAddress        string `envconfig:"MAIL_FROM_EMAIL" default:"m@onegeek.com.au"`
	MailSubjectPrefix      string `envconfig:"MAIL_SUBJECT_PREFIX" default:""`
}

// NewConfig returns an application configuration struct.
func NewConfig() *Config {
	c := Config{}
	envconfig.Process("", &c)

	// Setup logging
	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "ERROR"},
		MinLevel: logutils.LogLevel(c.LogLevel),
		Writer:   os.Stderr,
	}
	log.SetOutput(filter)
	log.Println("[DEBUG] config:", &c)

	// Setup DB
	c.DB = db.GetDatabase(c.ConnectionString)

	globalConfig = &c

	return &c
}

// GetGlobalConfig gets the current config for the app.
func GetGlobalConfig() *Config {
	return globalConfig
}
