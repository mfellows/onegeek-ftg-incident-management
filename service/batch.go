package service

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/utils"
)

// BatchService provides a utility for sending mail.
type BatchService struct {
}

// StartBatchService creates and starts our daily batch.
func StartBatchService() error {
	melbourneTime, _ := time.LoadLocation("Australia/Melbourne")
	cron := utils.Cron{
		// When: time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 20, 58, 00, 0, melbourneTime),
		When: time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 17, 0, 0, 0, melbourneTime),
		Name: "daily batch email to leadership",
		Next: 24 * time.Hour,
		Job: func() {
			fmt.Println("[INFO] sending daily batch email")

			// cURL the local endpoint
			client := &http.Client{}
			req, err := http.NewRequest("POST", fmt.Sprintf("http://localhost:%s/batch/email", os.Getenv("PORT")), nil)
			if err != nil {
				log.Println("[ERROR] cannot create new request", err)
			}
			_, err = client.Do(req)
			if err != nil {
				log.Println("[ERROR] cannot call batch endpoint", err)
			}
		},
	}
	cron.Start()
	return nil
}
