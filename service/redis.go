package service

import (
	"fmt"
	"log"
	"net/url"

	"gopkg.in/redis.v3"
)

// RedisClient returns a handle to a Redis connection.
func RedisClient(uri string, db int) *redis.Client {
	redisInfo, err := url.Parse(uri)
	if err != nil {
		log.Panic("rmq uri provided is invalid:", err)
	}

	password, _ := redisInfo.User.Password()
	options := &redis.Options{
		Network:  "tcp",
		Addr:     fmt.Sprintf(redisInfo.Host),
		DB:       int64(db),
		Password: password,
	}
	return redis.NewClient(options)
}
