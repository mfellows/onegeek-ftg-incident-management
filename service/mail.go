package service

import (
	"encoding/json"
	"errors"
	"log"
	"strings"
	"time"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"

	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/models"
	"github.com/adjust/rmq"
)

// MailService provides a utility for sending mail.
type MailService struct {
	c         *config.Config
	redisConn rmq.Connection
}

// NewMailService creates a MailService instance.
func NewMailService() *MailService {
	c := config.GetGlobalConfig()
	return &MailService{
		c:         c,
		redisConn: rmq.OpenConnectionWithRedisClient("mailservice", RedisClient(c.RedisURL, 2)),
	}
}

// func isDuplicateEmail(string email, list ...[]*mail.Email) bool {
func isDuplicateEmail(email string, list ...[]*mail.Email) bool {
	for _, l := range list {
		for _, e := range l {
			log.Println("[INFO] comparing email addresses:", email, e.Address)
			if strings.ToLower(e.Address) == strings.ToLower(email) {

				return true
			}
		}
	}
	return false
}

// Consume consumes a mail message and delivers it via Sendgrid.
func (m *MailService) Consume(delivery rmq.Delivery) {
	var incomingMail models.Mail
	var err error
	if err = json.Unmarshal([]byte(delivery.Payload()), &incomingMail); err != nil {
		log.Println("[ERROR] unable to read mail from queue:", err)
		delivery.Reject()
		return
	}

	log.Println("[DEBUG] mail consumer received payload:", incomingMail)

	message := mail.NewV3Mail()
	personalisation := mail.NewPersonalization()
	personalisation.Subject = incomingMail.Subject

	message.SetFrom(mail.NewEmail(m.c.MailFromName, m.c.MailFromAddress))
	for _, a := range incomingMail.To[:] {
		personalisation.AddTos(mail.NewEmail(a.Name, a.Address))
	}

	// (B)CCs
	personalisation.AddBCCs(mail.NewEmail(m.c.MailCCRecipientName, m.c.MailCCRecipientAddress))

	for _, a := range incomingMail.Cc[:] {
		// Ensure no duplicates
		if !isDuplicateEmail(a.Address, []*mail.Email{&mail.Email{Address: m.c.MailCCRecipientAddress}}, personalisation.To, personalisation.CC, personalisation.BCC) {
			personalisation.AddCCs(&mail.Email{Address: a.Address})
		}
	}

	// Apply the personalisations + content
	message.AddPersonalizations(personalisation)
	message.AddContent(mail.NewContent("text/html", incomingMail.Body))

	// Make the API call
	request := sendgrid.GetRequest(m.c.MailAPIKey, "/v3/mail/send", "https://api.sendgrid.com")
	request.Method = "POST"
	request.Body = mail.GetRequestBody(message)
	response, err := sendgrid.API(request)

	if err != nil {
		log.Println("[ERROR] error sending mail:", err)
	} else {
		log.Println("[INFO] mail with subject:", incomingMail.Subject, ", to:", incomingMail.To, ", was sent with status:", response.StatusCode, response.Body)
	}

	delivery.Ack()
}

// SendMessage queues an email up for sending (async).
func (m *MailService) SendMessage(mail models.Mail) error {
	mailQueue := m.redisConn.OpenQueue("mail")

	bytes, err := json.Marshal(mail)
	if err != nil {
		return err
	}

	if mailQueue.PublishBytes(bytes) {
		log.Println("[DEBUG] mail sent!")
	} else {
		log.Println("[DEBUG] unable to send mail")
		return errors.New("Unable to send mail :(")
	}

	return nil
}

// SetupWorker monitors incoming messages and sends out emails.
func (m *MailService) SetupWorker() {
	log.Println("[DEBUG] setting up mail service")
	mailQueue := m.redisConn.OpenQueue("mail")
	mailQueue.StartConsuming(100, 500*time.Millisecond)
	mailQueue.AddConsumer("mail worker", m)
}
