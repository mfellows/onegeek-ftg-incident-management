package main

import (
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/config"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/controllers"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/db"
	"bitbucket.org/mfellows/onegeek-ftg-incident-management/service"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	c := config.NewConfig()

	// Setup Mail Service
	mailService := service.NewMailService()

	// Perform migrations
	db.Migrate(c.ConnectionString)
	studentsController := controllers.NewStudentsController(c)
	teachersController := controllers.NewTeachersController(c)
	incidentsController := controllers.NewIncidentsController(c)
	classesController := controllers.NewClassRoomController(c)
	SchoolController := controllers.NewSchoolController(c)
	syncController := controllers.NewSyncController(c, mailService)
	mailController := controllers.NewMailController(c)
	batchController := controllers.NewBatchController(c, mailService)
	preferencesController := controllers.NewPreferencesController(c)

	// Routes
	r.GET("/students", studentsController.List)
	r.GET("/students/:id", studentsController.Get)
	r.GET("/students/:id/incidents", studentsController.GetIncidentsByStudent)
	r.GET("/students/:id/teacher", studentsController.GetTeacherByStudent)
	r.POST("/students", studentsController.Create)
	r.PUT("/students", studentsController.Update)

	r.GET("/teachers", teachersController.List)
	r.GET("/teachers/:id", teachersController.Get)
	r.POST("/teachers", teachersController.Create)
	r.PUT("/teachers", teachersController.Update)

	r.GET("/classes", classesController.ListCurrentYear)
	r.GET("/classes/:year/:id", classesController.Get)
	r.GET("/classes/:year", classesController.List)
	r.POST("/classes", classesController.Create)
	r.PUT("/classes", classesController.Update)
	r.DELETE("/classes/:id", classesController.Delete)

	r.POST("/sync", syncController.Sync)

	r.GET("/incidents/:id", incidentsController.Get)
	r.GET("/incidents", incidentsController.List)
	r.PUT("/incidents", incidentsController.Update)
	r.POST("/incidents", incidentsController.Create)
	r.DELETE("/incidents/:id", incidentsController.Delete)

	r.GET("/batch/incidents/today", incidentsController.ListToday)
	r.POST("/batch/email", batchController.SendDigestCommunications)
	r.POST("/mail", mailController.Create)

	r.GET("/preferences", preferencesController.List)
	r.GET("/preferences/:id", preferencesController.Get)
	r.DELETE("/preferences/:id", preferencesController.Delete)
	r.POST("/preferences", preferencesController.Create)
	r.PUT("/preferences", preferencesController.Update)

	r.GET("/schools", SchoolController.List)
	r.GET("/schools/:id", SchoolController.Get)
	r.DELETE("/schools/:id", SchoolController.Delete)
	r.POST("/schools", SchoolController.Create)
	r.PUT("/schools", SchoolController.Update)

	// TODO: move most externally facing APIs under /school root
	// comment to test something
	//       OR /<domain>/:school_id/:id? style
	r.GET("/school/:id/preferences", preferencesController.ListBySchool)

	go mailService.SetupWorker()

	r.Run()
}
